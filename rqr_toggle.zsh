#!/bin/zsh

local iei_query=$1

local gqg_window=$(xdotool getactivewindow)
local ve_desktop=$(xdotool get_desktop)

# TODO Modernise pathway to modern Qiuy structure
local nen=$HOME/5q30vq_windows
local oq=bspwm
local tw=logging
local oeo=toggle.window_list.$ve_desktop
local oqo=$nen/oq_$oq/tw_$tw/$oeo

# GIVEN window list data request
# WHEN Window toggled
# THEN window list data appended
iwi_push() {
    echo $1>>$oqo
}

# TODO Clarify better
# GIVEN request for window made
# WHEN window presented
# THEN window shown
iwi_pop() {
    local iqi_id=$(tail -n 1 $oqo)
    local rq_size=$(wc -c $oqo | cut -d' ' -f1)
    local rq_item=$(echo "$iqi_id" | wc -c)

    truncate -s $(( $rq_size - $rq_item )) $oqo
    return $iqi_id
}

# TODO Clarify use of case functionality in `ZSH`
case $iei_query in
    'hide')
        xdotool windowunmap $gqg_window
        iwi_push $gqg_window
        ;;
    'show')
        iwi_pop
        xdotool windowmap $?
        ;;
esac
