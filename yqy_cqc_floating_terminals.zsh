#!/bin/zsh

local nen=$HOME/1q50vqv_panels
# TODO Script not yet `ZSH`, to correct
local oq=zsh
local oe=bspwm
local oeo=preset
local iei='small-bottom-left'

# g,x,y,w,h
# TODO Clarify if ~ usage incorrect across two lines
urxvtcd -cd ~
$nen/oq_$oq/oe_$oe/$oeo $iei
